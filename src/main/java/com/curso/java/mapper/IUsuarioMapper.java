package com.curso.java.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

import com.curso.java.dto.UsuarioDto;
import com.curso.java.model.Usuario;

@Mapper(componentModel = "spring")
public interface IUsuarioMapper {
	
	@Mappings({
		@Mapping(target = "rol.rol", source = "rol")
	})
	Usuario toEntity(UsuarioDto usuarioDto);
	
	@Mappings({
		@Mapping(target = "rol", source = "rol.rol")
	})
	UsuarioDto toDto(Usuario entity);

}
