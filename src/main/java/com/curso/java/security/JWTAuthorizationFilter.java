package com.curso.java.security;

import java.io.IOException;import org.apache.tomcat.util.bcel.Const;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

@Component
public class JWTAuthorizationFilter extends OncePerRequestFilter {
	
	@Autowired
	private JWTUtil jwtUtil;
	
	@Autowired
	private UserDetailsService userDetailsService;
	
	@Override
	protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
	
		try {
			
			String token = parseJwt(request);
			if(token != null && jwtUtil.validateJwtToken(token) ) {
				String userName = jwtUtil.getUserNameFromJWTToken(token);
				
				UserDetails ud = userDetailsService.loadUserByUsername(userName);
				
				UsernamePasswordAuthenticationToken authToken = new UsernamePasswordAuthenticationToken(ud, null,ud.getAuthorities());
				authToken.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));
				
				SecurityContextHolder.getContext().setAuthentication(authToken);
			}
			
		} catch (Exception e) {
			// TODO: handle exception
			System.out.print("Error autenticando " + e);
		}
		
		filterChain.doFilter(request, response);
	}
	
	private String parseJwt(HttpServletRequest request) {
		String headerAuth = request.getHeader(Constant.HEADER_AUTHORIZACION_KEY);
		
		if(headerAuth != null && !headerAuth.isEmpty() && headerAuth.startsWith(Constant.TOKEN_BEARER_PREFIX)) {
			return headerAuth.substring(7, headerAuth.length());
		}
		else
			return null;
		
	}

}
