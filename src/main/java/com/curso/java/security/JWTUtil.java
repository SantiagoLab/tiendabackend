package com.curso.java.security;

import java.util.Collection;
import java.util.Date;
import java.util.stream.Collectors;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.stereotype.Component;

import com.curso.java.model.Usuario;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.MalformedJwtException;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.UnsupportedJwtException;
import io.jsonwebtoken.security.Keys;

@Component
public class JWTUtil {
	public String generaJWTToken(Usuario usuario)
	{
		Collection<?> roles = usuario.getAuthorities().stream().map(GrantedAuthority::getAuthority)
				.collect(Collectors.toList());
		
		String token = Jwts.builder()
				.setIssuedAt(new Date())
				.setIssuer(Constant.ISSUER_INFO)
				.setSubject(usuario.getUsername())
				.setExpiration(new Date(System.currentTimeMillis() + Constant.TOKEN_EXPIRATION_TIME))
				.claim(Constant.AUTHORITIES, roles)
				.signWith(Keys.hmacShaKeyFor(Constant.SUPER_SECRET_KEY.getBytes()),SignatureAlgorithm.HS512)
				.compact();
		
		return token;
	}
	
	public String getUserNameFromJWTToken(String token) {
		try {
			Claims claims = Jwts.parserBuilder()
					.setSigningKey(Constant.SUPER_SECRET_KEY.getBytes())
					.build()
					.parseClaimsJws(token).getBody();
			
			return claims.getSubject();
			
		} catch (Exception e) {
			// TODO: handle exception
			return null;
		}
	}
	
	public boolean validateJwtToken(String authToken) {
		System.out.println("authToken " + authToken);
		try {
			Jwts.parserBuilder()
			.setSigningKey(Constant.SUPER_SECRET_KEY.getBytes())
			.build()
			.parseClaimsJws(authToken);
			return true;
		} catch (SecurityException e) {
			System.out.println("Invalid JWT signature: {} " + e.getMessage());
		} catch (MalformedJwtException e) {
			System.out.println("Invalid JWT token: {} " + e.getMessage());
		} catch (ExpiredJwtException e) {
			System.out.println("JWT token is expired: {} " + e.getMessage());
		} catch (UnsupportedJwtException e) {
			System.out.println("JWT token is unsupported: {} " + e.getMessage());
		} catch (IllegalArgumentException e) {
			System.out.println("JWT claims string is empty: {} " + e.getMessage());
		}
 
		return false;
	}

}
