package com.curso.java.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.curso.java.model.Articulo;

@Repository
public interface IArticuloRepository extends JpaRepository<Articulo, Long> {

	Optional<Articulo> findByNombre(@Param("nombre") String nombre);
}
