package com.curso.java.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.curso.java.model.IngresoProducto;

@Repository
public interface IIngresoProductoRepository extends JpaRepository<IngresoProducto, Long> {

}
