package com.curso.java.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.curso.java.model.Compra;

@Repository
public interface ICompraRepository extends JpaRepository<Compra, Long> {

	Optional<Compra> findByNumeroDocumento(@Param("numeroDocumento") String numeroDocumento);
}
