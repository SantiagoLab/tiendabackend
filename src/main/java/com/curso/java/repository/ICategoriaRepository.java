package com.curso.java.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.curso.java.model.Categoria;

@Repository
public interface ICategoriaRepository extends JpaRepository<Categoria, String> {

	Optional<Categoria> findByNombre(@Param("nombre") String nombre);
}
