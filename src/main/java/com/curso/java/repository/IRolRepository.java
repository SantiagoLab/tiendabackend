package com.curso.java.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.curso.java.model.Rol;

@Repository
public interface IRolRepository extends JpaRepository<Rol, Long> {

	@Query("select r from Rol r where r.rol = :rol")
	Optional<Rol> findByRol(@Param("rol") String rol);
}
