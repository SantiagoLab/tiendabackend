package com.curso.java.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.curso.java.model.Categoria;
import com.curso.java.service.CategoriaService;
import com.curso.java.util.WebUtil;

@RestController
@RequestMapping("/categoria")
public class CategoriaController extends BaseController {

	@Autowired
	private CategoriaService categoriaService;
	
	@GetMapping()
	public ResponseEntity<?> getAll() {
		List<Categoria> categorias = categoriaService.getAll();
		
		if(categorias == null || categorias.isEmpty())
			return ResponseEntity.noContent().build();
		
		return ResponseEntity.ok(categorias);
	}
	
	@PostMapping()
	public ResponseEntity<?> save(@RequestBody @Validated Categoria categoria, BindingResult result) {
		
		if(result.hasErrors())
			return WebUtil.getErrors(result);

		Categoria nuevoRol = categoriaService.save(categoria);
		if(nuevoRol == null)
			//return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Registro ya existe");
			throw new DataIntegrityViolationException("Registro ya existe " + categoria.getNombre());
		
		return ResponseEntity.status(HttpStatus.CREATED).body(nuevoRol);
	}
	
	@PutMapping()
	public ResponseEntity<?> update(@RequestBody Categoria categoria) {
		Categoria rolUpdate = categoriaService.update(categoria);
		if(rolUpdate == null)
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Registro no existe");
		
		return ResponseEntity.status(HttpStatus.CREATED).body(rolUpdate);
	}
	
	@DeleteMapping(path = "/{id}")
	public ResponseEntity<?> delete(@PathVariable(value = "id") String id) {
		if(categoriaService.delete(id))
			return ResponseEntity.status(HttpStatus.OK).build();
		
		return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
	}
}
