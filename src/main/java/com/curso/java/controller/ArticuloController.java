package com.curso.java.controller;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.curso.java.model.Articulo;
import com.curso.java.service.ArticuloService;
import com.curso.java.util.WebUtil;

@RestController
@RequestMapping("/articulo")
public class ArticuloController extends BaseController {

	@Autowired
	private ArticuloService articuloService;
	
	@GetMapping()
	public ResponseEntity<?> getAll() {
		List<Articulo> articulos = articuloService.getAll();
		
		if(articulos == null || articulos.isEmpty())
			return ResponseEntity.noContent().build();
		
		return ResponseEntity.ok(articulos);
	}
	
	@PostMapping()
	public ResponseEntity<?> save(@RequestBody @Validated Articulo articulo, BindingResult result) {
		
		if(result.hasErrors())
			return WebUtil.getErrors(result);
		
		articulo.setFechaCreacion(new Date());

		Articulo nuevoRol = articuloService.save(articulo);
		if(nuevoRol == null)
			//return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Registro ya existe");
			throw new DataIntegrityViolationException("Registro ya existe " + articulo.getNombre());
		
		return ResponseEntity.status(HttpStatus.CREATED).body(nuevoRol);
	}
	
	@PutMapping()
	public ResponseEntity<?> update(@RequestBody Articulo articulo) {
		Articulo rolUpdate = articuloService.update(articulo);
		if(rolUpdate == null)
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Registro no existe");
		
		return ResponseEntity.status(HttpStatus.CREATED).body(rolUpdate);
	}
	
	@DeleteMapping(path = "/{id}")
	public ResponseEntity<?> delete(@PathVariable(value = "id") long id) {
		if(articuloService.delete(id))
			return ResponseEntity.status(HttpStatus.OK).build();
		
		return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
	}
}
