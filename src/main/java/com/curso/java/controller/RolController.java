package com.curso.java.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.curso.java.model.Rol;
import com.curso.java.service.RolService;
import com.curso.java.util.WebUtil;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;


@RestController
@RequestMapping("/rol")
public class RolController extends BaseController {
	
	@Autowired
	private RolService rolService;
	
	@GetMapping()
	public ResponseEntity<?> getAll() {
		List<Rol> roles = rolService.getAll();
		
		if(roles == null || roles.isEmpty())
			return ResponseEntity.noContent().build();
		
		return ResponseEntity.ok(roles);
	}
	
	@PostMapping()
	public ResponseEntity<?> save(@RequestBody @Validated Rol rol, BindingResult result) {
		
		if(result.hasErrors())
			return WebUtil.getErrors(result);

		Rol nuevoRol = rolService.save(rol);
		if(nuevoRol == null)
			//return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Registro ya existe");
			throw new DataIntegrityViolationException("Registro ya existe " + rol.getRol());
		
		return ResponseEntity.status(HttpStatus.CREATED).body(nuevoRol);
	}
	
	@PutMapping()
	public ResponseEntity<?> update(@RequestBody Rol rol) {
		Rol rolUpdate = rolService.update(rol);
		if(rolUpdate == null)
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Registro no existe");
		
		return ResponseEntity.status(HttpStatus.CREATED).body(rolUpdate);
	}
	
	@DeleteMapping(path = "/{id}")
	public ResponseEntity<?> delete(@PathVariable(value = "id") long id) {
		if(rolService.delete(id))
			return ResponseEntity.status(HttpStatus.OK).build();
		
		return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
	}

}
