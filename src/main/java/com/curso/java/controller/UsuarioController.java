package com.curso.java.controller;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.curso.java.dto.UsuarioDto;
import com.curso.java.dto.UsuarioResponse;
import com.curso.java.model.Usuario;
import com.curso.java.service.UsuarioService;
import com.curso.java.util.WebUtil;


@RestController
@RequestMapping("/usuario")
public class UsuarioController {
	
	@Autowired
	private UsuarioService usuarioService;
	
	@GetMapping()
	public ResponseEntity<?> getAll() {
		List<Usuario> Usuarioes = usuarioService.getAll();
		
		if(Usuarioes == null || Usuarioes.isEmpty())
			return ResponseEntity.noContent().build();
		
		return ResponseEntity.ok(Usuarioes);
	}
	
	@PostMapping()
	public ResponseEntity<?> save(@RequestBody @Validated Usuario usuario, BindingResult result) {
		
		if(result.hasErrors())
			return WebUtil.getErrors(result);

		UsuarioResponse nuevoUsuario = usuarioService.save(usuario);
		if(nuevoUsuario == null)
			//return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Registro ya existe");
			throw new DataIntegrityViolationException("Registro ya existe " + usuario.getEmail());
		
		return ResponseEntity.status(HttpStatus.CREATED).body(nuevoUsuario);
	}
	
	@PutMapping()
	public ResponseEntity<?> update(@RequestBody Usuario usuario) {
		Usuario usuarioUpdate = usuarioService.update(usuario);
		if(usuarioUpdate == null)
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Registro no existe");
		
		return ResponseEntity.status(HttpStatus.CREATED).body(usuarioUpdate);
	}
	
	@DeleteMapping(path = "/{id}")
	public ResponseEntity<?> Eliminar(@PathVariable(value = "id") long id) {
		if(usuarioService.delete(id))
			return ResponseEntity.status(HttpStatus.OK).build();
		
		return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
	}
	
	@GetMapping(path = "/findall")
	public ResponseEntity<?> findAll() {
		List<UsuarioDto> Usuarioes = usuarioService.findAll();
		
		if(Usuarioes == null || Usuarioes.isEmpty())
			return ResponseEntity.noContent().build();
		
		return ResponseEntity.ok(Usuarioes);
	}

}
