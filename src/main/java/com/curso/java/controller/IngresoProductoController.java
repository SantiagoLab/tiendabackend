package com.curso.java.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.curso.java.model.IngresoProducto;
import com.curso.java.service.IngresoProductoService;
import com.curso.java.util.WebUtil;

@RestController
@RequestMapping("/ingreso")
public class IngresoProductoController {

	@Autowired
	private IngresoProductoService ingresoProductoService;
	
	@GetMapping()
	public ResponseEntity<?> getAll() {
		List<IngresoProducto> roles = ingresoProductoService.getAll();
		
		if(roles == null || roles.isEmpty())
			return ResponseEntity.noContent().build();
		
		return ResponseEntity.ok(roles);
	}
	
	@PostMapping()
	public ResponseEntity<?> save(@RequestBody @Validated IngresoProducto ingresoProducto, BindingResult result) {
		
		if(result.hasErrors())
			return WebUtil.getErrors(result);

		IngresoProducto nuevoRol = ingresoProductoService.save(ingresoProducto);
		if(nuevoRol == null)
			throw new DataIntegrityViolationException("Registro no existe ");
		
		return ResponseEntity.status(HttpStatus.CREATED).body(nuevoRol);
	}
	
	@PutMapping()
	public ResponseEntity<?> update(@RequestBody IngresoProducto ingresoProducto) {
		IngresoProducto rolUpdate = ingresoProductoService.update(ingresoProducto);
		if(rolUpdate == null)
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Registro no existe");
		
		return ResponseEntity.status(HttpStatus.CREATED).body(rolUpdate);
	}
	
	@DeleteMapping(path = "/{id}")
	public ResponseEntity<?> delete(@PathVariable(value = "id") long id) {
		if(ingresoProductoService.delete(id))
			return ResponseEntity.status(HttpStatus.OK).build();
		
		return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
	}
}
