package com.curso.java.dto;


public class UsuarioResponse {
	
	private String email;
	private String nombres;
	private String telefono;
	private String direccion;
	private String rol;
	
	public UsuarioResponse(String email, String nombres, String telefono, String direccion, String rol) {
		super();
		this.email = email;
		this.nombres = nombres;
		this.telefono = telefono;
		this.direccion = direccion;
		this.rol = rol;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getNombres() {
		return nombres;
	}
	public void setNombres(String nombres) {
		this.nombres = nombres;
	}
	public String getTelefono() {
		return telefono;
	}
	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}
	public String getDireccion() {
		return direccion;
	}
	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}
	public String getRol() {
		return rol;
	}
	public void setRol(String rol) {
		this.rol = rol;
	}
	
	

}
