package com.curso.java.dto;

public class UserDto {
	
	private String nombres;
	private String userName;
	private String token;
	private String rol;
	public UserDto(String nombres, String userName, String token, String rol) {
		super();
		this.nombres = nombres;
		this.userName = userName;
		this.token = token;
		this.rol = rol;
	}
	public String getNombres() {
		return nombres;
	}
	public void setNombres(String nombres) {
		this.nombres = nombres;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getToken() {
		return token;
	}
	public void setToken(String token) {
		this.token = token;
	}
	public String getRol() {
		return rol;
	}
	public void setRol(String rol) {
		this.rol = rol;
	}
	
	

}
