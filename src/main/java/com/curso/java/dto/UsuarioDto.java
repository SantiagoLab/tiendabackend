package com.curso.java.dto;

import java.util.Date;

import com.curso.java.model.Rol;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class UsuarioDto {

	private long id;

	private String email;
	private String nombres;
	private String apellidos;
	private String password;
	private String telefono;
	private String direccion;
	private Date fecha;
	private String rol;

}
