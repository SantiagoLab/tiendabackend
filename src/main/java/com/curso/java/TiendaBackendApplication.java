package com.curso.java;

import java.util.Date;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.curso.java.dto.UsuarioDto;
import com.curso.java.model.Usuario;

@SpringBootApplication
public class TiendaBackendApplication {

	public static void main(String[] args) {
		SpringApplication.run(TiendaBackendApplication.class, args);
		
		
		Usuario usuario = new Usuario();
		usuario.setApellidos("apelldos");
		System.out.println(usuario);
		
		UsuarioDto user = UsuarioDto.builder().apellidos("nombre")
				.build();
		
		System.out.println(user);
		
	}

}
