package com.curso.java.model;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonFormat;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.OneToMany;

@Entity
public class Compra {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;
	@JsonFormat(pattern = "dd/MM/yyyy")
	private Date fechaCreacion;
	private BigDecimal subtotal;
	private BigDecimal iva;
	private BigDecimal total;
	private String numeroDocumento;
	
	@ManyToOne
	@JoinColumn(name = "idUsuario")
	private Usuario usuario;
	
	@OneToMany(mappedBy = "compra", cascade = CascadeType.ALL)
	private List<DetalleCompra> detalleCompraList;
	
	public Compra()
	{
		
	}	

	public Compra(long id, Date fechaCreacion, BigDecimal subtotal, BigDecimal iva, BigDecimal total,
			String numeroDocumento, Usuario usuario, List<DetalleCompra> detalleCompraList) {
		super();
		this.id = id;
		this.fechaCreacion = fechaCreacion;
		this.subtotal = subtotal;
		this.iva = iva;
		this.total = total;
		this.numeroDocumento = numeroDocumento;
		this.usuario = usuario;
		this.detalleCompraList = detalleCompraList;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Date getFechaCreacion() {
		return fechaCreacion;
	}

	public void setFechaCreacion(Date fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}

	public BigDecimal getSubtotal() {
		return subtotal;
	}

	public void setSubtotal(BigDecimal subtotal) {
		this.subtotal = subtotal;
	}

	public BigDecimal getIva() {
		return iva;
	}

	public void setIva(BigDecimal iva) {
		this.iva = iva;
	}

	public BigDecimal getTotal() {
		return total;
	}

	public void setTotal(BigDecimal total) {
		this.total = total;
	}

	public String getNumeroDocumento() {
		return numeroDocumento;
	}

	public void setNumeroDocumento(String numeroDocumento) {
		this.numeroDocumento = numeroDocumento;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public List<DetalleCompra> getDetalleCompraList() {
		return detalleCompraList;
	}

	public void setDetalleCompraList(List<DetalleCompra> detalleCompraList) {
		this.detalleCompraList = detalleCompraList;
	}
	
	
}
