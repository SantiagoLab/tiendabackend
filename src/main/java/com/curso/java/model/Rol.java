package com.curso.java.model;

import java.util.HashSet;
import java.util.Set;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.OneToMany;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;

@Entity
public class Rol {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;
	
	@NotNull
	@Size(max = 10)
	private String rol;
	
	@Size(min = 1, max = 50, message = "Descripcion entre 1 y 50 panas")
	private String descripcion;
	
	@OneToMany(mappedBy = "rol")
	private Set<Usuario> usuarios = new HashSet<Usuario>();
	
	public Rol() {
		
	}

	public Rol(long id, String rol, String descripcion) {
		super();
		this.id = id;
		this.rol = rol;
		this.descripcion = descripcion;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getRol() {
		return rol;
	}

	public void setRol(String rol) {
		this.rol = rol;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	
	
}
