package com.curso.java.model;

import java.util.HashSet;
import java.util.Set;

import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.OneToMany;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;

@Entity
public class Categoria {

	@Id
	private String id;
	
	@NotNull
	@NotBlank
	private String nombre;
	
	//@OneToMany(mappedBy = "categoria")
	//private Set<Articulo> articulos = new HashSet<Articulo>();
	
	public Categoria() {
		
	}

	public Categoria(String id, @NotNull @NotBlank String nombre) {
		super();
		this.id = id;
		this.nombre = nombre;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	
}
