package com.curso.java.model;

import java.math.BigDecimal;
import java.util.Date;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;

@Entity
public class IngresoProducto {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;
	private int cantidad;
	private Date fechaIngreso;
	private BigDecimal total;
	@ManyToOne
	@JoinColumn(name = "idArticulo")
	private Articulo articulo;
	@ManyToOne
	@JoinColumn(name = "idUsuario")
	private Usuario usuario;
	
	public IngresoProducto() {
		
	}

	public IngresoProducto(long id, int cantidad, Date fechaIngreso, BigDecimal total, Articulo articulo,
			Usuario usuario) {
		super();
		this.id = id;
		this.cantidad = cantidad;
		this.fechaIngreso = fechaIngreso;
		this.total = total;
		this.articulo = articulo;
		this.usuario = usuario;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public int getCantidad() {
		return cantidad;
	}

	public void setCantidad(int cantidad) {
		this.cantidad = cantidad;
	}

	public Date getFechaIngreso() {
		return fechaIngreso;
	}

	public void setFechaIngreso(Date fechaIngreso) {
		this.fechaIngreso = fechaIngreso;
	}

	public BigDecimal getTotal() {
		return total;
	}

	public void setTotal(BigDecimal total) {
		this.total = total;
	}

	public Articulo getArticulo() {
		return articulo;
	}

	public void setArticulo(Articulo articulo) {
		this.articulo = articulo;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}
	
	
	
}
