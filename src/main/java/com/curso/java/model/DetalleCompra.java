package com.curso.java.model;

import java.math.BigDecimal;

import com.fasterxml.jackson.annotation.JsonIgnore;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;

@Entity
public class DetalleCompra {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;
	private int cantidad;
	private BigDecimal subtotal;
	private BigDecimal total;
	
	@ManyToOne
	@JoinColumn(name = "idCompra")
	@JsonIgnore
	private Compra compra;
	
	@ManyToOne
	@JoinColumn(name = "idArticulo")
	private Articulo articulo;
	
	public DetalleCompra() {
		
	}

	public DetalleCompra(long id, int cantidad, BigDecimal subtotal, BigDecimal total, Compra compra,
			Articulo articulo) {
		super();
		this.id = id;
		this.cantidad = cantidad;
		this.subtotal = subtotal;
		this.total = total;
		this.compra = compra;
		this.articulo = articulo;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public int getCantidad() {
		return cantidad;
	}

	public void setCantidad(int cantidad) {
		this.cantidad = cantidad;
	}

	public BigDecimal getSubtotal() {
		return subtotal;
	}

	public void setSubtotal(BigDecimal subtotal) {
		this.subtotal = subtotal;
	}

	public BigDecimal getTotal() {
		return total;
	}

	public void setTotal(BigDecimal total) {
		this.total = total;
	}

	public Compra getCompra() {
		return compra;
	}

	public void setCompra(Compra compra) {
		this.compra = compra;
	}

	public Articulo getArticulo() {
		return articulo;
	}

	public void setArticulo(Articulo articulo) {
		this.articulo = articulo;
	}
	
	
}
