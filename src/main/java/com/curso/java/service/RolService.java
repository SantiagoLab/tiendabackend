package com.curso.java.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.curso.java.model.Rol;
import com.curso.java.repository.IRolRepository;

@Service
public class RolService implements IRolService {

	@Autowired
	IRolRepository iRolRepository;
	@Override
	public List<Rol> getAll() {		
		return iRolRepository.findAll();		
	}
	
	@Override
	public Rol save(Rol rol) {
		if(!iRolRepository.findByRol(rol.getRol()).isPresent())
			return iRolRepository.save(rol);
		
		return null;		
	}

	@Override
	public Rol update(Rol rol) {
		if(iRolRepository.findById(rol.getId()).isPresent())
		{
			return iRolRepository.save(rol);
		}
		
		return null;
	}

	@Override
	public boolean delete(long id) {
		
		
		if(iRolRepository.findById(id).isPresent()) {
			iRolRepository.deleteById(id);
			return true;
		}
		
		return false;
	}

}
