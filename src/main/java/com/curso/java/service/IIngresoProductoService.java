package com.curso.java.service;

import java.util.List;

import com.curso.java.model.IngresoProducto;

public interface IIngresoProductoService {

	List<IngresoProducto> getAll();
	IngresoProducto save(IngresoProducto ingresoProducto);
	IngresoProducto update(IngresoProducto ingresoProducto);
	boolean delete(long id);
}
