package com.curso.java.service;

import java.util.List;

import com.curso.java.model.Rol;

public interface IRolService {

	List<Rol> getAll();
	Rol save(Rol rol);
	Rol update(Rol rol);
	boolean delete(long id);
}
