package com.curso.java.service;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.curso.java.model.Articulo;
import com.curso.java.model.IngresoProducto;
import com.curso.java.model.Usuario;
import com.curso.java.repository.IIngresoProductoRepository;

@Service
public class IngresoProductoService implements IIngresoProductoService {

	@Autowired
	IIngresoProductoRepository iIngresoProductoRepository;
	@Autowired
	ArticuloService articuloService;
	@Autowired
	UsuarioService usuarioService;
	
	
	@Override
	public List<IngresoProducto> getAll() {		
		return iIngresoProductoRepository.findAll();		
	}
	
	@Override
	public IngresoProducto save(IngresoProducto ingresoProducto) {
		
		Articulo articulo = articuloService.findById(ingresoProducto.getArticulo().getId()).orElse(null);
		
		if(articulo != null)
		{
			articulo.setCantidad(ingresoProducto.getCantidad() + articulo.getCantidad());
			articuloService.update(articulo);
			
			ingresoProducto.setFechaIngreso(new Date());
			ingresoProducto.setTotal(articulo.getPrecio().multiply(new BigDecimal(ingresoProducto.getCantidad())));
			
			Usuario usuario = usuarioService.findById(ingresoProducto.getUsuario().getId());
			if(usuario != null)
				ingresoProducto.setUsuario(usuario);
			
			return iIngresoProductoRepository.save(ingresoProducto);
		}
		
		return null;		
	}

	@Override
	public IngresoProducto update(IngresoProducto ingresoProducto) {
		if(iIngresoProductoRepository.findById(ingresoProducto.getId()).isPresent())
		{
			return iIngresoProductoRepository.save(ingresoProducto);
		}
		
		return null;
	}

	@Override
	public boolean delete(long id) {
		
		
		if(iIngresoProductoRepository.findById(id).isPresent()) {
			iIngresoProductoRepository.deleteById(id);
			return true;
		}
		
		return false;
	}

}
