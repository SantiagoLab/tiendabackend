package com.curso.java.service;

import java.util.List;
import java.util.Optional;

import com.curso.java.model.Articulo;

public interface IArticuloService {

	List<Articulo> getAll();
	Articulo save(Articulo articulo);
	Articulo update(Articulo articulo);
	boolean delete(long id);
	Optional<Articulo> findById(long id);
}
