package com.curso.java.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.curso.java.model.Articulo;
import com.curso.java.repository.IArticuloRepository;

@Service
public class ArticuloService implements IArticuloService {

	@Autowired
	IArticuloRepository iArticuloRepository;
	@Override
	public List<Articulo> getAll() {		
		return iArticuloRepository.findAll();		
	}
	
	@Override
	public Articulo save(Articulo articulo) {
		if(!iArticuloRepository.findByNombre(articulo.getNombre()).isPresent())
			return iArticuloRepository.save(articulo);
		
		return null;		
	}

	@Override
	public Articulo update(Articulo articulo) {
		if(iArticuloRepository.findById(articulo.getId()).isPresent())
		{
			return iArticuloRepository.save(articulo);
		}
		
		return null;
	}

	@Override
	public boolean delete(long id) {
		
		
		if(iArticuloRepository.findById(id).isPresent()) {
			iArticuloRepository.deleteById(id);
			return true;
		}
		
		return false;
	}

	@Override
	public Optional<Articulo> findById(long id) {
		return iArticuloRepository.findById(id);
	}
}
