package com.curso.java.service;

import java.util.List;

import com.curso.java.model.Categoria;

public interface ICategoriaService {
	
	List<Categoria> getAll();
	Categoria save(Categoria categoria);
	Categoria update(Categoria categoria);
	boolean delete(String id);

}
