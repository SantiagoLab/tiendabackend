package com.curso.java.service;

import java.util.List;

import com.curso.java.dto.UsuarioDto;
import com.curso.java.dto.UsuarioResponse;
import com.curso.java.model.Usuario;

public interface IUsuarioService {

	List<Usuario> getAll();
	List<UsuarioDto> findAll();
	UsuarioResponse save(Usuario usuario);
	Usuario update(Usuario usuario);
	boolean delete(long id);
}
