package com.curso.java.service;

import java.util.List;

import com.curso.java.model.Compra;

public interface ICompraService {

	List<Compra> getAll();
	Compra save(Compra compra);
	Compra update(Compra compra);
	boolean delete(long id);
}
