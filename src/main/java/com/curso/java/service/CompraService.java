package com.curso.java.service;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.curso.java.model.Articulo;
import com.curso.java.model.Compra;
import com.curso.java.model.DetalleCompra;
import com.curso.java.repository.ICompraRepository;

@Service
public class CompraService implements ICompraService {

	@Autowired
	ICompraRepository iCompraRepository;
	
	@Autowired
	ArticuloService articuloService;
	
	@Override
	public List<Compra> getAll() {		
		return iCompraRepository.findAll();		
	}
	
	@Override
	public Compra save(Compra compra) {
		
		compra.setNumeroDocumento(String.valueOf(new Date().getTime()));
		
		if(!iCompraRepository.findByNumeroDocumento(compra.getNumeroDocumento()).isPresent()) {
			compra.setFechaCreacion(new Date());
			
			for (DetalleCompra detalle : compra.getDetalleCompraList()) {
				
				Optional<Articulo> articulo = articuloService.findById(detalle.getArticulo().getId());
				if(articulo.isPresent()) {
					articulo.get().setCantidad(articulo.get().getCantidad() - detalle.getCantidad());
					articuloService.update(articulo.get());
				}
				
				detalle.setCompra(compra);
			}
			
			return iCompraRepository.save(compra);
		}
		
		return null;		
	}

	@Override
	public Compra update(Compra compra) {
		if(iCompraRepository.findById(compra.getId()).isPresent())
		{
			return iCompraRepository.save(compra);
		}
		
		return null;
	}

	@Override
	public boolean delete(long id) {
		
		
		if(iCompraRepository.findById(id).isPresent()) {
			iCompraRepository.deleteById(id);
			return true;
		}
		
		return false;
	}
}
