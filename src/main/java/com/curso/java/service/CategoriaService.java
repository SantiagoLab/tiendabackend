package com.curso.java.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.curso.java.model.Categoria;
import com.curso.java.repository.ICategoriaRepository;

@Service
public class CategoriaService implements ICategoriaService {

	@Autowired
	ICategoriaRepository iCategoriaRepository;
	@Override
	public List<Categoria> getAll() {		
		return iCategoriaRepository.findAll();		
	}
	
	@Override
	public Categoria save(Categoria categoria) {
		if(!iCategoriaRepository.findByNombre(categoria.getNombre()).isPresent())
			return iCategoriaRepository.save(categoria);
		
		return null;		
	}

	@Override
	public Categoria update(Categoria categoria) {
		if(iCategoriaRepository.findById(categoria.getId()).isPresent())
		{
			return iCategoriaRepository.save(categoria);
		}
		
		return null;
	}

	@Override
	public boolean delete(String id) {
		
		
		if(iCategoriaRepository.findById(id).isPresent()) {
			iCategoriaRepository.deleteById(id);
			return true;
		}
		
		return false;
	}

}
