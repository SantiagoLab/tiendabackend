package com.curso.java.service;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.curso.java.dto.UsuarioDto;
import com.curso.java.dto.UsuarioResponse;
import com.curso.java.mapper.IUsuarioMapper;
import com.curso.java.model.Usuario;
import com.curso.java.repository.IRolRepository;
import com.curso.java.repository.IUsuarioRepository;

@Service
public class UsuarioService implements IUsuarioService {

	@Autowired
	IUsuarioRepository iUsuarioRepository;
	@Autowired
	IRolRepository iRolRepository;
	@Autowired
	IUsuarioMapper iUsuarioMapper;
	
	
	@Override
	public List<Usuario> getAll() {
		return iUsuarioRepository.findAll();
	}

	@Override
	public UsuarioResponse save(Usuario usuario) {
		usuario.setFecha(new Date());
		if(!iUsuarioRepository.findByEmail(usuario.getEmail()).isPresent()) {
			
			Usuario user = iUsuarioRepository.save(usuario);
			user.setRol(iRolRepository.findById(user.getRol().getId()).orElse(null));
			return toDto(user);
		}
		
		return null;
	}

	@Override
	public Usuario update(Usuario usuario) {
		if(iUsuarioRepository.findById(usuario.getId()).isPresent())
		{
			return iUsuarioRepository.save(usuario);
		}
		
		return null;
	}

	@Override
	public boolean delete(long id) {
		if(iUsuarioRepository.findById(id).isPresent()) {
			iUsuarioRepository.deleteById(id);
			return true;
		}
		
		return false;
	}
	
	private UsuarioResponse toDto(Usuario usuario) {
		return new UsuarioResponse(usuario.getEmail(), usuario.getNombres(), usuario.getTelefono(), usuario.getDireccion(), usuario.getRol().getRol());
	}
	
	public Usuario findById(long id) {
		return iUsuarioRepository.findById(id).orElse(null);
	}
	
	@Override
	public List<UsuarioDto> findAll(){
		List<Usuario> usuarios = iUsuarioRepository.findAll();
		
		return usuarios.stream().map(user -> iUsuarioMapper.toDto(user)).collect(Collectors.toList());
	}

}
